import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { Zoo } from './zoo.entity';
import { ZooService } from './zoo.service';

@Controller('zoo')
export class ZooController {
  constructor(private zooService: ZooService) {}
  @Get()
  index(): Promise<Zoo[]> {
    return this.zooService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id): Promise<Zoo> {
    return this.zooService.findZoo(id);
  }
  @Post('create')
  async create(@Body() zooData: Zoo): Promise<any> {
    return this.zooService.create(zooData);
  }

  @Put(':id/update')
  async update(@Param('id') id, @Body() zooData: Zoo): Promise<any> {
    zooData.id = Number(id);
    if (this.zooService.findZoo(zooData.id)) {
      console.log('Update #' + zooData.id);
      return this.zooService.update(zooData);
    } else {
      return 'Error';
    }
  }

  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    const zoo = await this.zooService.delete(id);
    if(zoo.affected == 1)
      return 'Success';
    else
      return 'Error';
  }
}

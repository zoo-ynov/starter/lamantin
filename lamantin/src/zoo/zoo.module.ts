import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZooService } from './zoo.service';
import { Zoo } from './zoo.entity';
import { ZooController } from './zoo.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Zoo])],
    providers: [ZooService],
    controllers: [ZooController],
})
export class ZooModule {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Zoo } from './zoo.entity';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class ZooService {
  constructor(
    @InjectRepository(Zoo)
    private readonly zooRepository: Repository<Zoo>,
  ) {}

  findAll(): Promise<Zoo[]> {
    return this.zooRepository.find();
  }
  findZoo(id):Promise<Zoo>{
    return this.zooRepository.findOne(id);
  }
  async create(zoo: Zoo): Promise<Zoo> {
    return await this.zooRepository.save(zoo);
  }

  async update(zoo: Zoo): Promise<UpdateResult> {
    return await this.zooRepository.update(zoo.id, zoo);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.zooRepository.delete(id);
  }
}

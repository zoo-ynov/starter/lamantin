import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Zoo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ })
  entrancePrice: number;

  @Column({ })
  location: string;
}
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { EnclosureService } from './enclosure.service';
import { Enclosure } from './enclosure.entity';

@Controller('enclosure')
export class EnclosureController {
  constructor(private enclosureService: EnclosureService) {}
  @Get()
  index(): Promise<Enclosure[]> {
    return this.enclosureService.findAll();
  }
  @Get(':id')
  findOne(@Param('id') id): Promise<Enclosure> {
    return this.enclosureService.findEnclosure(id);
  }
  @Post('create')
  async create(@Body() enclosureData: Enclosure): Promise<any> {
    return this.enclosureService.create(enclosureData);
  }

  @Put(':id/update')
  async update(
    @Param('id') id,
    @Body() enclosureData: Enclosure,
  ): Promise<any> {
    enclosureData.id = Number(id);
    if (this.enclosureService.findEnclosure(enclosureData.id)) {
      console.log('Update #' + enclosureData.id);
      return this.enclosureService.update(enclosureData);
    }
  }

  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    const enclosure = await this.enclosureService.delete(id);
    if(enclosure.affected == 1)
      return 'Success';
    else
      return 'Error';
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnclosureService } from './enclosure.service';
import { Enclosure } from './enclosure.entity';
import { EnclosureController } from './enclosure.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Enclosure])],
    controllers: [EnclosureController],
    providers: [EnclosureService],
    exports: [EnclosureService]
})
export class EnclosureModule {}

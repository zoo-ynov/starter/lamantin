import { Test, TestingModule } from '@nestjs/testing';
import { EnclosureController } from './enclosure.controller';

describe('Enclosure Controller', () => {
  let controller: EnclosureController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EnclosureController],
    }).compile();

    controller = module.get<EnclosureController>(EnclosureController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

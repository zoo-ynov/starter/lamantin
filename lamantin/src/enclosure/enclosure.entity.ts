import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Enclosure {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ })
  animalsQuantity: number;

  @Column({ })
  capacityMax: number;

  @Column({ })
  idZoo: number;
}
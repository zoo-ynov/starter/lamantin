import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Enclosure } from './enclosure.entity';
import { UpdateResult, DeleteResult } from 'typeorm';

import { AnimalService } from '../animal/animal.service';
import { Animal } from '../animal/animal.entity';

@Injectable()
export class EnclosureService {
  private animalService: AnimalService;
  constructor(
    @InjectRepository(Enclosure)
    private readonly enclosureRepository: Repository<Enclosure>,
  ) {}

  findAll(): Promise<Enclosure[]> {
    return this.enclosureRepository.find();
  }
  findEnclosure(id): Promise<Enclosure> {
    return this.enclosureRepository.findOne(id);
  }
  async create(enclosure: Enclosure): Promise<Enclosure> {
    return await this.enclosureRepository.save(enclosure);
  }

  async update(enclosure: Enclosure): Promise<UpdateResult> {
    return await this.enclosureRepository.update(enclosure.id, enclosure);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.enclosureRepository.delete(id);
  }

  findEnclosureCapacity(id, animalsQuantity): Promise<Enclosure[]> {
    let actualCapacity = this.enclosureRepository.find();
    return actualCapacity;
  }
 
}

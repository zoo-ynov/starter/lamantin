import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { Employee } from './employee.entity';

@Controller('employee')
export class EmployeeController {
  constructor(private employeeService: EmployeeService) {}
  @Get()
  index(): Promise<Employee[]> {
    return this.employeeService.findAll();
  }
  @Get(':id')
  findOne(@Param('id') id): Promise<Employee> {
    return this.employeeService.findEmployee(id);
  }

  @Post('create')
  async create(@Body() employeeData: Employee): Promise<any> {
    return this.employeeService.create(employeeData);
  }

  @Put(':id/update')
  async update(@Param('id') id, @Body() employeeData: Employee): Promise<any> {
    employeeData.id = Number(id);
    if(this.employeeService.findEmployee( employeeData.id)){
    console.log('Update #' + employeeData.id);
    return this.employeeService.update(employeeData);
    }
  }

  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    const employee = await this.employeeService.delete(id);
    if(employee.affected == 1)
      return 'Success';
    else
      return 'Error';
  }
}

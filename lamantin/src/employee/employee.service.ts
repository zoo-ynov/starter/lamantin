import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from './employee.entity';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
  ) {}

  findAll(): Promise<Employee[]> {
    return this.employeeRepository.find();
  }
  findEmployee(id): Promise<Employee> {
    return this.employeeRepository.findOne(id);
  }
  async create(contact: Employee): Promise<Employee> {
    return await this.employeeRepository.save(contact);
  }

  async update(contact: Employee): Promise<UpdateResult> {
    return await this.employeeRepository.update(contact.id, contact);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.employeeRepository.delete(id);
  }
}

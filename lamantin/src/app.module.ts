import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZooModule } from './zoo/zoo.module';
import { EmployeeModule } from './employee/employee.module';
import { EnclosureModule } from './enclosure/enclosure.module';
import { AnimalModule } from './animal/animal.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'root',
      password: 'root',
      database: 'lamantin',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    ZooModule,
    EmployeeModule,
    EnclosureModule,
    AnimalModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

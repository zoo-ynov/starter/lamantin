import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { AnimalService } from './animal.service';
import { Animal } from './animal.entity';
import { EnclosureService } from '../enclosure/enclosure.service';

@Controller('animal')
export class AnimalController {
  constructor(
    private animalService: AnimalService,
    private enclosureService: EnclosureService,
  ) {}

  @Get()
  index() {
    return this.animalService.findAll();
  }
  @Get(':id')
  findOne(@Param('id') id) {
    return this.animalService.findAnimal(id);
  }
  @Post('create')
  async create(@Body() animalData: Animal) {
    let Animals = await this.animalService.findAnimalInEnclosure(animalData.idEnclosure);
    let enclosure = await this.enclosureService.findEnclosure(animalData.idEnclosure);
    if(enclosure.capacityMax>enclosure.animalsQuantity){
      if((Animals.length && Animals[0].foodType === animalData.foodType) || !Animals.length){
        enclosure.animalsQuantity ++;
        this.enclosureService.update(enclosure);
        return this.animalService.create(animalData);
      }
      else {
        return "le régime alimentaire des animaux n'est pas compatible";
      }
    }
    else {
      return "l'enclos est plein";
    }
  }

  @Put(':id/update')
  async update(@Param('id') id, @Body() animalData: Animal) {
    animalData.id = Number(id);
    if (await this.animalService.findAnimal(animalData.id)) {
      console.log('Update #' + animalData.id);
      return this.animalService.update(animalData);
    }
  }

  @Delete(':id/delete')
  async delete(@Param('id') id) {
    const animal = await this.animalService.delete(id);
    if(animal.affected == 1)
      return 'Success';
    else
      return 'Error';
  }
}

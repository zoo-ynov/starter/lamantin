import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Animal } from './animal.entity';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class AnimalService {
  constructor(
    @InjectRepository(Animal)
    private readonly animalRepository: Repository<Animal>,
  ) {}

  findAll(): Promise<Animal[]> {
    return this.animalRepository.find();
  }

  findAnimal(id): Promise<Animal> {
    return this.animalRepository.findOne(id);
  }
  async create(animal: Animal): Promise<Animal> {
    return await this.animalRepository.save(animal);
  }

  async update(animal: Animal): Promise<UpdateResult> {
    return await this.animalRepository.update(animal.id, animal);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.animalRepository.delete(id);
  }

  findAnimalInEnclosure(id): Promise<Animal[]>{
    return this.animalRepository.find({ where : { idEnclosure: id } });
  }
}

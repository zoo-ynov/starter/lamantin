import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalService } from './animal.service';
import { Animal } from './animal.entity';
import { AnimalController } from './animal.controller';
import { EnclosureModule } from 'src/enclosure/enclosure.module';

@Module({
  imports: [TypeOrmModule.forFeature([Animal]), EnclosureModule],
  controllers: [AnimalController],
  providers: [AnimalService],
  exports: [AnimalService]
})
export class AnimalModule {}

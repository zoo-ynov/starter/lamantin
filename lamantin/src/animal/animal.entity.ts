import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Animal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ length: 100 })
  variety: string;

  @Column({ })
  environment: string;

  @Column({ })
  foodType: string;

  @Column({ })
  idEnclosure: number;
}